# Repository for VHDL course (O. Sentieys)

To download the files used during the VHDL course:
```
cd
git clone https://gitlab.inria.fr/sentieys/vhdl_course.git
cd vhdl_course/src
```
or go to <https://gitlab.inria.fr/sentieys/vhdl_course/-/tree/master> and download sources,

or (files may not be up-to-date)

```
cd
wget http://people.rennes.inria.fr/Olivier.Sentieys/teach/VHDL.zip
unzip VHDL.zip
cd VHDL
```

Then check the md files depending on the VHDL simulation tool you plan to use (ModelSim, GHDL, EDAPlayGround).

# Slides of the course

* [Main slides](https://gitlab.inria.fr/sentieys/vhdl_course/-/blob/master/slides/VHDL_Logic_Synthesis.pdf)
* [Appendix slides](https://gitlab.inria.fr/sentieys/vhdl_course/-/blob/master/slides/VHDL_Logic_Synthesis_Appendix.pdf)

