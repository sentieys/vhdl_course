
# GHDL

GHDL is an open-source simulator for the VHDL language. GHDL allows you to compile and execute your VHDL code directly in your PC on Windows, Linux or MacOS. Combined with a GUI-based wave viewer (GTKWave) and a good VHDL text editor (gedit, emacs, etc.), GHDL is a very powerful tool for writing, testing and simulating your VHDL code.

## Installation 
See <http://ghdl.free.fr/site/pmwiki.php?n=Main.Installation>
* Linux: `sudo apt-get install ghdl gtkwave`
* Mac OS X: `brew install --cask ghdl gtkwave` ou `sudo port install ghdl gtkwave`
* Windows: [installer](http://ghdl.free.fr/ghdl-installer-0.29.1.exe)

# Simulating with GHDL

Compiling and analyzing vhdl files (design and testbench) with `ghdl -a <file.vhdl>`.
Elaborating the testbench with `ghdl -e <test_name>`.
Launching simulation with `ghdl -r <test_name> --stop-time=50ns --vcd=<vcd_file.vcd>`. Simulation results are dumped into a vcd file.

The full script to simulate the Half Adder is given below.

```
ghdl -a HalfAdder.vhdl 
ghdl -a sim_HalfAdder.vhdl
ghdl -e test_ha
ghdl -r test_ha --stop-time=50ns --vcd=sim_HalfAdder.vcd
```

The User Guide of GHDL is [here](http://ghdl.free.fr/site/pmwiki.php?n=Main.UserGuide).  A pdf version is [here](https://buildmedia.readthedocs.org/media/pdf/ghdl-devfork/latest/ghdl-devfork.pdf).

# Displaying waveforms with [GTKWave](http://gtkwave.sourceforge.net/)

* Launch `gtkwave`
* Open the vcd file
* Select test_ha and then drop the signals you want to display into the Wave frame.
* You should see something like this:

![gtkwave](https://gitlab.inria.fr/sentieys/vhdl_course/-/raw/master/doc/gtkwave.png "gtkwave output")
