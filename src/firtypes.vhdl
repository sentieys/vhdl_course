package firtypes is

   constant N: integer := 8;
   constant Nbit: integer := 8;

   type mem is array (0 to N-1) of integer range -128 to +127;
   constant ROM: Mem := (13,22,-38,51,51,-38,22,13); 
   -- Values of coefficients in fixed-point format (8,0,7)
   -- (0.1015625, 0.171875, -0.296875, 0.3984375, 0.3984375, -0.296875, 0.171875, 0.1015625)

   type states is  (repos, init, mac, mac2, acc, acc2);

	constant clock_cycle: time := 5 ns;
   constant sampling_period: time := 20*clock_cycle;

end firtypes;

