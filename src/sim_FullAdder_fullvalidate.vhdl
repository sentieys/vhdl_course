library IEEE;
use IEEE.std_logic_1164.all;

-- tesbench of FA

entity Test_FA2 is 
end Test_FA2;

architecture test of Test_FA2  is

   component FullAdder 
      port (a, b, c_in: in std_logic;
            sum, carry: out std_logic);
   end component;

   signal SA, SB, SSum, SCin , SCout: std_logic := '0';

   constant cycle: time := 10 ns;

   -- configuration
   for U1:FullAdder use entity work.FullAdder(behavioral);

   type Stimuli is array (0 to 4) of std_logic;
   type ArrayStimuli is array (0 to 7) of Stimuli;
   constant FA_table: ArrayStimuli 
    := (('0', '0', '0', '0', '0'),
	('0', '0', '1', '0', '1'),
	('0', '1', '0', '0', '1'),
	('0', '1', '1', '1', '0'),
	('1', '0', '0', '0', '1'),
	('1', '0', '1', '1', '0'),
	('1', '1', '0', '1', '0'),
	('1', '1', '1', '1', '0')); -- add an error to test assertion


begin

   -- Component Instantiation
   U1: FullAdder port map(a=>SA, b=>SB, sum=>SSum, c_in=>SCin, carry=>SCout);	

   Simulation: process 
   begin

     wait for cycle;
     for i in FA_table'range(1) loop
	SA <= FA_table(i)(0);
	SB <= FA_table(i)(1);
	SCin <= FA_table(i)(2);
	wait for cycle;
	assert (SCout = FA_table(i)(3)) 
	report "Problem on carry"
	severity warning;
	assert (SSum = FA_table (i)(4)) 
	report "Problem on sum" 
	severity warning;
     end loop;
   wait;


   end process;
end test;

