entity Ex2 is
  port( A,B,C,D,clk: in bit;
        S: out bit );
end Ex2;
architecture RTL of Ex2 is
  signal int2: bit;
begin
process(clk)
  variable int1: std_logic;
begin
  if (clk'event and clk='1') then
    int1 := A nand B;
    int2 <= C nor D;
    S <= int1 nand int2;
  end if;
end process;
end RTL;

