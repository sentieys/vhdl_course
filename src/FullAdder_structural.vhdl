library IEEE;
use IEEE.std_logic_1164.all;
entity FullAdder is
port (a, c_in, b: in std_logic;
      sum, carry: out std_logic);
end entity FullAdder;

architecture structural of FullAdder is

  component HalfAdder is
    port (a, b: in std_logic; 
          sum, carry: out std_logic);
  end component HalfAdder;

  signal s1, s2, s3: std_logic;

begin

...

end structural;

