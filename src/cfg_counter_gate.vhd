library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.ALL;

configuration counter_gate_cfg of bench_gate is

	for test
		for cpt:counter
			use entity work.counter(SYN_RTL);
		end for;
	end for;

end counter_gate_cfg;
