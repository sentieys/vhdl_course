

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use IEEE.STD_LOGIC_arith.all;
use IEEE.STD_LOGIC_SIGNED.all;

library work;
use work.firtypes.all;

entity UC is
        PORT (  Rstb : in Std_Logic;
                Clk : in Std_Logic ;
                Cpt_rom : in integer range 0 to N-1;
                start : in Std_Logic;
                endof : out Std_Logic;
                R1R2 : out Std_Logic;
                R3, R4, clear : out std_logic;
                En_RAM : out std_logic);
end UC;


architecture RTL of UC is

      SIGNAL CurrentState, NextState : states;
begin

 	synchronous : PROCESS(Clk, Rstb)
        BEGIN
                IF Rstb = '0' THEN
                        CurrentState <= repos;
                ELSIF Clk'event AND Clk = '1' THEN
                        CurrentState <= NextState;
                END IF;
        END PROCESS;

 ...


END RTL;  

