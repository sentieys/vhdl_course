library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.ALL;

configuration counter_cfg of bench is

	for test
		for cpt:counter
			use entity work.counter(RTL);
		end for;
	end for;

end counter_cfg;
