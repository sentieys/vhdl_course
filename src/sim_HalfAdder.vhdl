library IEEE;
use IEEE.std_logic_1164.all;

-- tesbench of HA

entity Test_HA is 
end Test_HA;

architecture test of Test_FA  is

   component HalfAdder 
      port (a, b: in std_logic;
            sum, carry: out std_logic);
   end HalfAdder;

   signal SA, SB, SSum , SCout: std_logic := '0';

   constant cycle: time := 10 ns;

   -- configuration
   for U1:HalfAdder use entity work.HalfAdder(behavioral);
   -- needs to be modified if architecture name is changed

begin

   -- Component Instantiation
   U1: HalfAdder port map(a=>SA, b=>SB, sum=>SSum, carry=>SCout);	

   Simulation: process 
   begin

	SA <= '0'; 
	SB <= '0'; 
	wait for cycle;
	
	SA <= '0';
	SB <= '1';
	wait for cycle;

	SA <= '1'; 
	SB <= '0'; 
	wait for cycle;
	
	SA <= '1';
	SB <= '1';
	wait for cycle;

	SA <= '0'; 
	SB <= '0'; 
	wait for cycle;
    
	wait;

   end process;
end test;


