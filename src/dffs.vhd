   --------------------------------------------------------------------
   -- A POSITIVE EDGE-TRIGGERED D FLIP-FLOP WITH ACTIVE LOW ASYNCHRONOUS 
   --                 RESET AND ACTIVE HIGH OUTPUT ENABLE
   --------------------------------------------------------------------
   library IEEE;
   use IEEE.STD_LOGIC_1164.all;
   entity REG1 is
   port (   Q    : out STD_LOGIC_VECTOR(3 downto 0);
            D    : in  STD_LOGIC_VECTOR(3 downto 0);
            CLK   : in  STD_LOGIC;
            Reset,Oe : in  STD_LOGIC  );
   end REG1;
   -- Premier style d'�criture 
   architecture REG1 of REG1 is
     signal Qi : STD_LOGIC_VECTOR(3 downto 0);
   begin
        DF : process(CLK)
        begin
          if CLK'event AND CLK = '1' then
              Qi <= D;
          end if;
        end process DF;

        CB : process(Reset,Oe,Qi)
        begin
          if Reset = '0' AND Oe = '1' then
             Q <= (others => '0');
          elsif Reset = '1' AND Oe = '0' then
             Q <= (others => 'Z');
          elsif Reset = '1' AND Oe = '1' then
             Q <= Qi;
          end if;
        end process CB;
   end REG1;

   -----------------------------------------------------------------------
   library IEEE;
   use IEEE.STD_LOGIC_1164.all;
   entity REG2 is
   port (   Q    : out STD_LOGIC_VECTOR(3 downto 0);
            D    : in  STD_LOGIC_VECTOR(3 downto 0);
            CLK   : in  STD_LOGIC;
            Reset,Oe : in  STD_LOGIC  );
   end REG2;
   -- Deuxieme style d'�criture
   architecture REG2 of REG2 is
     signal Qi : STD_LOGIC_VECTOR(3 downto 0);
   begin
        DF : process(CLK)
        begin
           if CLK'event AND CLK = '1' then
              Qi <= D;
           end if;
        end process DF;

        CB : process(Reset,Oe,Qi)
        begin
          if Reset = '0' AND Oe = '1' then
             Q <= (others => '0');
          elsif Reset = '1' AND Oe = '0' then
             Q <= (others => 'Z');
          elsif Reset = '0' AND Oe = '0' then
             Q <= (others => '0');
          else
             Q <= Qi;
          end if;
        end process CB;
   end REG2;

   -----------------------------------------------------------------------
   library IEEE;
   use IEEE.STD_LOGIC_1164.all;
   entity REG3 is
   port (   Q    : out STD_LOGIC_VECTOR(3 downto 0);
            D    : in  STD_LOGIC_VECTOR(3 downto 0);
            CLK   : in  STD_LOGIC;
            Reset,Oe : in  STD_LOGIC  );
   end REG3;
   -- Troisieme style d'�criture
   architecture REG3 of REG3 is
     signal Qi : STD_LOGIC_VECTOR(3 downto 0);
   begin
        DF : process(CLK, Reset)
        begin
          if Reset = '0' then
              Qi <= (others => '0');
          elsif CLK'event AND CLK = '1' then
             if Oe = '0' then
                  Q <= (others => 'Z');
             else
                  Qi <= D;
             end if;
          end if;
        end process DF;

        CB : process(Reset,Oe,Qi)
        begin
          if Reset = '0' AND Oe = '1' then
             Q <= (others => '0');
          elsif Reset = '1' AND Oe = '0' then
             Q <= (others => 'Z');
          elsif Reset = '0' AND Oe = '0' then
             Q <= (others => '0');
          else
             Q <= Qi;
          end if;
        end process CB;

   end REG3;

   -----------------------------------------------------------------------
   library IEEE;
   use IEEE.STD_LOGIC_1164.all;
   entity REG4 is
   port (   Q    : out STD_LOGIC_VECTOR(3 downto 0);
            D    : in  STD_LOGIC_VECTOR(3 downto 0);
            CLK   : in  STD_LOGIC;
            Reset,Oe : in  STD_LOGIC  );
   end REG4;
   -- Quatrieme style d'�criture  
   architecture REG4 of REG4 is
     signal Qi : STD_LOGIC_VECTOR(3 downto 0);
   begin
        DF : process(CLK, Reset)
        begin
          if Reset = '0' then
              Qi <= (others => '0');
          elsif CLK'event AND CLK = '1' then
              Qi <= D;
          end if;
        end process DF;

        CB : process(Oe,Qi)
        begin
          if Oe = '0' then
             Q <= (others => 'Z');
          else
             Q <= Qi;
          end if;
        end process CB;
   end REG4;

