library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

library work;
use work.firtypes.all;

entity fir_top is
	port (clk, rstb, new_sample: in Std_Logic;
          xn: in Std_Logic_Vector (Nbit-1 downto 0) := (others=> '0'); 
          yn: out Std_Logic_Vector (Nbit-1 downto 0) := (others=> '0')
	     );	
end fir_top;

architecture specification of fir_top is
begin
	process
		variable tmp: integer;
        variable tmp_signed: signed(2*Nbit-1 downto 0);
	    variable x: mem := (others => 0);
	begin
        wait until new_sample'event and new_sample = '1';
		tmp := to_integer(signed(xn)) * ROM(0);
		for i in 1 to N-1 loop
			tmp := tmp + x(i) * ROM(i);
	    end loop;
        tmp_signed := to_signed(tmp,2*Nbit);
        yn <= std_logic_vector(tmp_signed(2*Nbit-2 downto Nbit-1));
		for i in N-1 downto 2 loop
			x(i) := x(i-1);
		end loop;
		x(1) := to_integer(signed(xn));
	end process;
end specification;