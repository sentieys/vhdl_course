entity clk_gen is
   port(phi1: out bit := '0');
end clk_gen;

architecture sim of clk_gen is
begin
	
clock_process: process is
  begin
    phi1 <= '1';
    wait for 0 ns;
    while true loop
      phi1 <= '1';
      wait for 10 ns;
      phi1 <= '0';
      wait for 10 ns;
    end loop;
  end process;

end sim;