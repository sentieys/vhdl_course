read_vhdl cpt_dec.vhd
current_design cpt_dec
create_clock -period 10.0 -waveform {0 5} [get_ports clk]
compile_ultra
report_constraint -all
report_timing
report_area
report_power
change_name -hierarchy -rule vhdl
write_sdf cpt_dec_gate.sdf
write -hierarchy -format vhdl -output cpt_dec_gate.vhd
