library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity test_fir_top is
end test_fir_top;

architecture test of test_fir_top is

signal Clk : std_logic :='0';
signal Rstb : std_logic :='0';
signal new_sample : std_logic :='0';
signal xn: std_logic_vector (7 downto 0) := (others =>'0');
signal yn : std_logic_vector (7 downto 0) := (others =>'0');
signal endof : std_logic := '0';

constant clock_cycle : TIME := 2 ns;

component fir_top is
	port ( clk, rstb, new_sample : in Std_Logic;
           xn : in Std_Logic_Vector (7 downto 0);   
           yn  : out Std_Logic_Vector (7 downto 0));	
end component fir_top;

begin

instance_fir : fir_top 
       PORT MAP (clk, rstb, new_sample, xn, yn);	
	
 clock : process
  begin
    clk <= '0';
    wait for 0 ns;
    while true loop
      clk <= '1';
      wait for clock_cycle/2;
      clk <= '0';
      wait for clock_cycle/2;
    end loop;
  end process;
  
  simulation : process
  begin
  	-- Valeur au reset
 	xn <= "00000000";
	new_sample <= '0';
        rstb <= '0';
	wait for clock_cycle;
        rstb <= '1';
	
	-- attente du 1er echantillon
	wait for clock_cycle*10;
	
	-- un premier echantillon de 8 bits egal a 0
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;

	-- arrivee d'1 echantillon de 8 bits egal a +1       
	xn <= "01111111";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;

	-- arrivee de 8 echantillons de 8 bits egal a 0
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
 	
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
	
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
	
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
		
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
		
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
		
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
		
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
		
	xn <= "00000000";
	new_sample <='1';
	wait for clock_cycle;
	new_sample <='0';
	wait for clock_cycle*10;
	

  	wait;
  end process;

end test;



