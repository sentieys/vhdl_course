library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.all;

library work;
use work.firtypes.all;

entity UM is

    port ( X : out Std_Logic_Vector(7 downto 0);
           H : out Std_Logic_Vector(7 downto 0);
           count : out integer range 0 to 7;
           rstb,clk : in Std_Logic;
           start : in Std_Logic;
           En_ROM : in Std_Logic;
           En_RAM : in Std_Logic;
           Xn : in Std_Logic_Vector(7 downto 0) );

end UM;

----------------------------------------------------------------------

architecture RTL OF UM is

	
      	signal ADR_ROM : integer range 0 to 7;
      	signal ADR_RAM : integer range 0 to 7;
      	signal RAM : mem := (others => 0);

begin

...

end RTL;


