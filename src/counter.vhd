library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity counter is
  generic (size :integer :=4);
  port ( reset : in Std_Logic; 
         clk   : in Std_Logic;
	      up    : in Std_Logic;
 	      load  : in Std_Logic;
	      value : in Std_Logic_Vector((size-1) downto 0);
         output: out Std_Logic_Vector((size-1) downto 0) );
end counter;

architecture RTL of counter is
   signal count : unsigned((size-1) downto 0);
begin
   synchrone : process (reset,clk) 
   begin
     if reset='1' then 
	     count<=(others => '0');
     elsif clk'event and clk='1' then
         if load='1' then 
		      count<=unsigned(value);
	      elsif up='1' then 
		      count <= count + 1;
	      else 
		      count <= count - 1;
	      end if ;
     end if ;
    end process ;
    output <= std_logic_vector(count);  
end RTL;
  
